﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObjects
{
    public class BaseModel
    {
        public int Id { get; set; }
        public string Error { get; set; }
    }
}
