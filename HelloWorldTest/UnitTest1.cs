using BusinessLayer;
using BusinessObjects;
using DataAccessLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HelloWorldTest
{
    [TestClass]
    public class UnitTest1
    {
        private static IDataAccess _dal;
        private static IHelloWorldService _helloWorldService;

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {

            _dal = new DataAccess();
            _helloWorldService = new HelloWorldService(_dal);
        }


        [TestMethod]
        public void GetHelloWorldTest()
        {
            HelloWorldModel result = _helloWorldService.GetHelloWorld();

            // check for successful request
            Assert.IsNotNull(result);

            // check for no errors
            Assert.IsTrue(string.IsNullOrEmpty(result.Error));

            // check for desired result
            Assert.AreEqual("Hello, World!", result.Value);
        }
    }
}
