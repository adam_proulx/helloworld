﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using BusinessObjects;
using DataAccessLayer;
using Microsoft.AspNetCore.Mvc;

namespace HelloWorldWebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ValuesController : Controller
    {
        private IHelloWorldService _helloWorld;

        public ValuesController(IHelloWorldService helloWorld)
        {
            _helloWorld = helloWorld;
        }

        //
        // Adam Proulx
        // 05/10/2018
        public string HelloWorld()
        {
            HelloWorldModel res = _helloWorld.GetHelloWorld();

            if (!string.IsNullOrEmpty(res.Error))
                return res.Error;

            return res.Value; 
        }


        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
