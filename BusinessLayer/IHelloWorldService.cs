﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public interface IHelloWorldService
    {
        HelloWorldModel GetHelloWorld();
    }
}
