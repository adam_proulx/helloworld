﻿using BusinessObjects;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public class HelloWorldService : IHelloWorldService
    {
        private IDataAccess _dal;

        public HelloWorldService(IDataAccess dal)
        {
            _dal = dal;
        }

        public HelloWorldModel GetHelloWorld()
        {
            return _dal.GetHelloWorld();
        }
    }
}
