﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer
{
    public interface IDataAccess
    {
        HelloWorldModel GetHelloWorld();
    }
}
