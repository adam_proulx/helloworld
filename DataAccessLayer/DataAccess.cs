﻿using BusinessObjects;
using System;
using System.Collections.Generic;

namespace DataAccessLayer
{
    public class DataAccess : IDataAccess
    {
        
        MockDataBase database = new MockDataBase();

        public HelloWorldModel GetHelloWorld()
        {
            HelloWorldModel result = new HelloWorldModel();

            try
            {
                result.Value = this.database.Table.GetValueOrDefault("Value");
            }
            catch(Exception ex)
            {
                result.Error = "An error ocurred.";

                throw ex;
            }

            return result;
        }
    }

    public class MockDataBase
    {
        public MockDataBase()
        {
            //
            // Sample data set to be used as simple database table
            this.Table = new Dictionary<string, string>()
            {
                { "Value", "Hello, World!" }
            };
        }

        public Dictionary<string, string> Table { get; set; }
    }
}
