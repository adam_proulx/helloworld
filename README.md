# README #

This is a .NET Core Web API Hello World project. In order to run the project, open the .sln file in Visual Studio and run it using the green play button at the top of the solution or using the shortcut ctrl+F5. 

Once the project is open and running, accessing the following URL should display the "Hello, World!" message in your browser.

URL: http://localhost:60793/api/values/HelloWorld

